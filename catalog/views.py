from django.contrib.auth.decorators import permission_required

from django.shortcuts import get_object_or_404
from django.http import HttpResponseRedirect
from django.urls import reverse
import datetime

from .forms import RenewBookForm

from django.shortcuts import render
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin

from .models import Book, Author, BookInstance, Genre

# Create your views here.

def index(request):
    """
    Funcion vista para la pagina inicio del sitio

    """

    # Genera contadores de algunos de los objetivos principales

    num_books = Book.objects.all().count()
    num_instances = Book.objects.all().count()

    # Libros disponibles (status = 'a')

    num_instances_available =  BookInstance.objects.filter(status__exact='a').count()
    num_authors = Author.objects.count()    # El 'all()' esta implicito por defecto.

    # Numero de visitas a esta vista, como se cuenta en la variable de sesion.

    num_visits = request.session.get('num_visits', 0)
    request.session['num_visits'] = num_visits+1

    #Renderiza la plantilla HTML index.html con los datos en la variable contexto

    return render(request,
                  'index.html',
                  context={
                      'num_books':num_books,
                      'num_instances':num_instances,
                      'num_instances_available':num_instances_available,
                      'num_authors':num_authors,
                      'num_visits' : num_visits
                  },            # num_visits appended
                  )

class BookListView(ListView):

    model = Book
    paginate_by = 10

#    context_object_name = 'my_book_list'  # your own name for the list as a template variable
#    queryset = Book.objects.filter(title__icontains='war')[:5] # Get 5 books containing the title war
#    template_name = 'books/my_arbitrary_template_name_list.html'  # Specify your own template name/locations
    


class BookDetailView(DetailView):

    model = Book


# Author Model


class AuthorListView(ListView):

    model = Author
    paginate_by = 10

class AuthorDetailView(DetailView):

    model = Author


class LoanedBooksByUserListView(LoginRequiredMixin, ListView):
    """

    Generic class-based view listing books on loan to current user.

    """

    model = BookInstance
    template_name = 'catalog/bookinstance_list_borrowed_user.html'
    paginate_by = 10

    def get_queryset(self):
        return BookInstance.objects.filter(borrower=self.request.user).filter(status__exact='o').order_by('due_back')

class LoanedBooksAllListView(PermissionRequiredMixin, ListView):
    """Generic class-based view listing all books on loan. Only visible to users with can_mark_returned permission."""

    model = BookInstance
    permission_required = 'catalog.can_mark_returned'
    template_name = 'catalog/bookinstance_list_borrowed_all.html'
    paginate_by = 10

    def get_queryset(self):
        return BookInstance.objects.filter(status__exact='o').order_by('due_back')


@permission_required('catalog.can_mark_returned')
def renew_book_librarian(request, pk):
    book_inst = get_object_or_404(BookInstance, pk = pk)

    # If this a POST request then process the Form Data

    if request.method == 'POST':

        # Create a form instance and populate it with data from the request (binding):
        form = RenewBookForm(request.POST)

        # Check if the form is valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required (here we just write it to the model due_back field)
            book_inst.due_back = form.cleaned_data['renewal_date']
            book_inst.save()

            # redirect to a new URL:

            return HttpResponseRedirect(reverse('all-borrowed'))

    # If this is a GET (or any other method) create the default form.

    else:
        proposed_renewal_date = datetime.date.today() + datetime.timedelta(weeks=3)
        form = RenewBookForm(initial={'renewal_date': proposed_renewal_date,})


    return render(request, 'catalog/book_renew_librarian.html', {'form': form, 'bookinst': book_inst})


# Author Model
class AuthorCreate(CreateView):
    model = Author
    fields = '__all__'
    initial = {'date_of_death': '05/01/2018',}

class AuthorUpdate(UpdateView):
    model = Author
    fields = ['first_name', 'last_name', 'date_of_birth', 'date_of_death']

class AuthorDelete(DeleteView):
    model = Author
    success_url = reverse_lazy('authors')


# Book Model
class BookCreate(PermissionRequiredMixin, CreateView):
    model = Book
    fields = '__all__'
    permission_required = 'catalog.can_mark_returned'

class BookUpdate(PermissionRequiredMixin, UpdateView):
    model = Book
    fields = '__all__'
    permission_required = 'catalog.can_mark_returned'

class BookDelete(PermissionRequiredMixin, DeleteView):
    model = Book
    success_url = reverse_lazy('books')
    permission_required = 'catalog.can_mark_returned'
